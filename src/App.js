
import React  from 'react';
import LoginPage from './component/LoginPage'
import { Route } from 'react-router-dom'

import "./component/style.css"
import Info from './component/info/infomation'
import Nav from './component/Navigator'
//import Footer from './component/Footer';

class App extends React.Component {
  render()
  {
    return (
      <div>
          <Route exact path="/" component={Home} />
          <Route path="/login" component={Login} />
      </div>
    )
  }
}
export default App

function Home() {
  return <div>
        <Nav/>
        <Info/>
  </div>
}
function Login() {
  return <LoginPage/>
}